import React from "react";
import DeleteIcon from '@mui/icons-material/Delete';

const Note = (props) => {
    return(
        <div 
            className="note"
            onClick={() => {
                props.onDelete(props.id);
            }}
        >
            <h1>{props.title}</h1>
            <p>{props.content}</p>
            <button><DeleteIcon /></button>
        </div>
    );
}

export default Note;