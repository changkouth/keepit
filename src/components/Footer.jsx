import React from "react";

function Footer() {

    const date = new Date();
    let year = date.getFullYear();
    const copyright = "Copyright";

    return (
        <footer>
            <p>{copyright} &copy; {year} </p>
        </footer>
    );
}

export default Footer;