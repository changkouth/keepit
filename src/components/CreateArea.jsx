import React, { useState } from "react";
import AddCircleIcon from '@mui/icons-material/AddCircle';
import Fab from '@material-ui/core/Fab';
import Zoom from '@mui/material/Zoom';

function CreateArea(props) {

    const [isExpanded, setExpanded] = useState(false);
   
    const [note, setNote] = useState({
        title: "",
        content: ""
    });
    
    function handleChange(event) {

        const {name, value} = event.target;

        setNote(prevNote => {
            return {
                ...prevNote,
                [name]: value
            };
        });
    }

    let submitNote = (event) => {
        props.onAdd(note);
        setNote({
            title: "",
            content: ""
        });
        event.preventDefault();
    }

    let expand = () => { setExpanded(true); }

    return(
        <div>
            <form className="create-note">
                {isExpanded && (
                    <input
                        onChange={handleChange}
                        name="title" 
                        placeholder="Title" 
                        type="text" 
                        value={note.title}
                    />
                )}

                <textarea
                    onClick={expand}
                    onChange={handleChange}
                    name="content" 
                    value={note.content}
                    placeholder="Take a note..." 
                    rows={isExpanded ? 3 : 1}
                />
                <Zoom in={isExpanded}>
                    <Fab onClick={submitNote}> <AddCircleIcon/> </Fab>
                </Zoom>
               
            </form>
        </div>
    );
}

export default CreateArea;